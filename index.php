<html>
    <head>
        <title>Atomic Project </title>
        <link rel="stylesheet" href="views/SEIP106475/bootstrap/css/bootstrap.css" />
    </head>
    <body>
        <div class="container" >
            <div class="jumbotron">
                <h1>BITM - Web App Dev - PHP</h1>
                <h3>Name: Ashikur Rahman</h3>
                <h4>SEIP:106475</h4>
            </div>
            
            <div class ="jumbotron">
                <h2>Projects</h2>
                <table class="table table-hover">
                    <thead>
                        <th>SL.No</th>
                        <th>Project Name</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>01</td>
                            <td> <a href="views/SEIP106475/BookTitle/index.php">Book Title</a></td>
                        </tr>
                       
                        <tr>
                            <td>02</td>
                            <td><a href="views/SEIP106475/CheckboxHobby/index.php">Check Box Mul</a></td>
                        </tr>
                        <tr>
                            <td>03</td>
                            <td><a href="views/SEIP106475/CheckboxTermsConditon/index.php">Terms </a></td>
                        </tr>
                        <tr>
                            <td>04</td>
                            <td> <a href="views/SEIP106475/DateOfBirth/index.php">Date</a></td>
                        </tr>
                        <tr>
                            <td>05</td>
                            <td> <a href="views/SEIP106475/ProfilePicture/index.php">Profile Photos</a></td>
                        </tr>
                        <tr>
                            <td>06</td>
                            <td> <a href="views/SEIP106475/SelectCity/index.php">City</a></td>
                        </tr>
                        <tr>
                            <td>07</td>
                            <td><a href="views/SEIP106475/SelectGender/index.php">Gender Select</a></td>
                        </tr>
                        <tr>
                            <td>08</td>
                            <td><a href="views/SEIP106475/Subscribe/index.php">Email</a></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td><a href="views/SEIP106475/SummaryOrganization/index.php">Summary Organization </a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
    </body>
</html>