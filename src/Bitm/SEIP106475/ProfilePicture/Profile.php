<?php

namespace app\Bitm\SEIP106475\ProfilePicture;

use \app\BITM\SEIP106475\Utility\Utility;

class Profile {

    public $id = "";
    public $name = "";
    public $photos = "";
    public $temp = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";

    public function __construct($data = false) {
       @ $this->id = $data['id'];
        @$this->name = $data['name'];
        @$this->photos = $_FILES['photo']['name'];
       @ $this->temp = $_FILES['photo']['tmp_name'];
    }

    public function index() {

        $books = array();

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `profilepictures`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $books[] = $row;
        }
        return $books;
    }

    public function store() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");



        move_uploaded_file($this->temp, "images/$this->photos");


        $query = "INSERT INTO `db_atomicproject`.`profilepictures` ( `name`,`photo`) VALUES ( '" . $this->name . "','" . $this->photos . "')";

        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function show($id = false) {
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `profilepictures` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);

        return $row;
    }

  public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        move_uploaded_file($this->temp, "images/$this->photos");
        
        $query = "UPDATE  `db_atomicproject`.`profilepictures` SET  `name` =  '" . $this->name . "' ,`photo` =  '" . $this->photos . "' WHERE  `profilepictures`.`id`=" . $this->id;

        //var_dump($query) or die();
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
     public function delete($id = null) {

       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomicproject`.`profilepictures` WHERE `profilepictures`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is deleted successfully.");
        } else {
            Utility::message(" Cannot delete.");
        }

        Utility::redirect('index.php');
    }

}
