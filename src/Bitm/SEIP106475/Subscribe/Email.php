<?php

namespace app\Bitm\SEIP106475\Subscribe;

use \app\BITM\SEIP106475\Utility\Utility;

class Email {

    public $id = "";
    public $name = "";
    public $email = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";

    public function __construct($data = false) {
        $this->name = $data['name'];
        $this->email = $data['email_address'];
       
    }
    
    public function index(){
        
        $emails = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `subscriptions`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $emails[] = $row;
        }
        return $emails;
    }
    
     public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `subscriptions` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }

    public function store() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `db_atomicproject`.`subscriptions` (`name`, `email_address`) VALUES ( '".$this->name."','".$this->email."')";
        
        $result = mysql_query($query);
        
        if($result){
            $_message = "Book title is added successfully.";
        }else{
            $_message =  "There is an error while saving data. Please try again later.";
        }
        
        Utility::redirect('index.php');
    }

    public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomicproject`.`subscriptions` SET `name` = '".$this->name."', `email_address` = '".$this->email."' WHERE `subscriptions`.`id` = ".$this->id;

       // var_dump($query) or die();
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }

     public function delete($id = null) {

       
       $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomicproject`.`subscriptions` WHERE `subscriptions`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is deleted successfully.");
        } else {
            Utility::message(" Cannot delete.");
        }

        Utility::redirect('index.php');
    }
    
    
}
