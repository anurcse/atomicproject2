<?php

namespace app\Bitm\SEIP106475\CheckboxHobby;

use \app\BITM\SEIP106475\Utility\Utility;

class Hobby {

    public $id = "";
    public $name = "";
    public $hobby1 = "";
    public $hobby2 = "";
    public $hobby3 = "";
    public $hobby4 = "";
    public $created = "";
    public $modified = "";
    public $created_by = "";
    public $modified_by = "";
    public $deleted_at = "";

    public function __construct($hobby = false) {
        $this->id = $hobby['id'];
        $this->name = $hobby['name'];
        $this->hobby1 = $hobby['hobby1'];
        $this->hobby2 = $hobby['hobby2'];
        $this->hobby3 = $hobby['hobby3'];
        $this->hobby4 = $hobby['hobby4'];
    }

    public function index() {

        $books = array();

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `hobbys`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $books[] = $row;
        }
        return $books;
    }

    public function create() {
        echo "I am Create form";
    }

    public function store() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "INSERT INTO `db_atomicproject`.`hobbys` ( `name`,`hoby1`,`hoby2`,`hoby3`,`hoby4`) VALUES ( '" . $this->name . "','" . $this->hobby1 . "','" . $this->hobby2 . "','" . $this->hobby3 . "','" . $this->hobby4 . "')";

        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function show($id = false) {
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `hobbys` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);

        return $row;
    }

       

    public function update() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        //  $query = "UPDATE `db_atomicproject`.`hobbys` SET `name` = '" . $this->name . "', `hoby1` = '" . $this->hobby1 . "'`hoby2` = '" . $this->hobby2 . "', `hoby3` = '" . $this->hobby3 . "', `hoby4` = '" . $this->hobby4 . "' WHERE `hobbys`.`id` = " . $this->id;
        //var_dump($query) or die();


        $query = "UPDATE  `db_atomicproject`.`hobbys` SET  `name` =  '" . $this->name . "' ,`hoby1` =  '" . $this->hobby1 . "',`hoby2` =  '" . $this->hobby2 . "',`hoby3` =  '" . $this->hobby3 . "',`hoby4` =  '" . $this->hobby4 . "' WHERE  `hobbys`.`id`=" . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Book title is edited successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }
    
     public function delete($id = null) {

       
         $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomicproject`.`hobbys` WHERE `hobbys`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is deleted successfully.");
        } else {
            Utility::message(" Cannot delete.");
        }

        Utility::redirect('index.php');
    }

}
