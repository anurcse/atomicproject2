<?php

namespace app\Bitm\SEIP106475\BookTitle;

use \app\BITM\SEIP106475\Utility\Utility;

class Book {

    public $id = "";
    public $title = "";
    public $author = "";

    //  public $created="";
    //   public $modified="";
    // public $created_by="";
    // public $modified_by="";
    //  public $deleted_at="";

    public function __construct($data = false) {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->author = $data['author'];
    }

    public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `books` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    public function index() {

        $books = array();

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `books`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $books[] = $row;
        }
        return $books;
    }

    public function store() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "INSERT INTO `db_atomicproject`.`books` ( `title`,`author`) VALUES ( '" . $this->title . "','" . $this->author . "')";

        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function delete($id = null) {

       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomicproject`.`books` WHERE `books`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is deleted successfully.");
        } else {
            Utility::message(" Cannot delete.");
        }

        Utility::redirect('index.php');
    }
    
    
     public function update(){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomicproject`.`books` SET `title` = '".$this->title."', `author` = '".$this->author."' WHERE `books`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }

}

