<?php
 namespace app\Bitm\SEIP106475\SelectCity;
 
 use \app\BITM\SEIP106475\Utility\Utility;
 
class City {
   public $id="";
    public $city="";
    public $name="";
    public $created="";
    public $modified="";
    public $created_by="";
    public $modified_by="";
    public $deleted_at="";
    
     public function __construct($city=false) {
         $this->id= $city['id'];
        $this->name = $city['name'];
        $this->city = $city['city'];
    }
    
    public function index(){
        
        $citys = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "SELECT * FROM `citys`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $citys[] = $row;
        }
        return $citys;
    }
    public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `citys` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    
     public function store() {

        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `db_atomicproject`.`citys` (`name`,`city`) VALUES ( '".$this->name."','".$this->city."')";
        
        $result = mysql_query($query);
        
        if($result){
            $_message = "Book title is added successfully.";
        }else{
            $_message =  "There is an error while saving data. Please try again later.";
        }
        
        Utility::redirect('index.php');
    }
   
    public function update(){
            
       $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `db_atomicproject`.`citys` SET `name` = '".$this->name."', `city` = '".$this->city."' WHERE `citys`.`id` = ".$this->id;
       // var_dump($query) or die();
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Book title is edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    public function delete($id = null) {

       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("db_atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `db_atomicproject`.`citys` WHERE `citys`.`id` = " . $id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Book title is deleted successfully.");
        } else {
            Utility::message(" Cannot delete.");
        }

        Utility::redirect('index.php');
    }
}
