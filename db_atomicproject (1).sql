-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 07:23 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE IF NOT EXISTS `birthdays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `name`, `date`) VALUES
(7, 'ssssssssssssssssrrrrrrrrrrrr', '2021-01-30');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`) VALUES
(3, 'fffffffffff', 'ffffffffffffffffffffff'),
(4, 'ddddddd', 'ddddddddddddd');

-- --------------------------------------------------------

--
-- Table structure for table `citys`
--

CREATE TABLE IF NOT EXISTS `citys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `citys`
--

INSERT INTO `citys` (`id`, `name`, `city`) VALUES
(4, 'Ashikur', 'Lamxpur');

-- --------------------------------------------------------

--
-- Table structure for table `genders`
--

CREATE TABLE IF NOT EXISTS `genders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `gender` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `genders`
--

INSERT INTO `genders` (`id`, `name`, `gender`) VALUES
(4, 'rrrrrrrrrrrrrrrr', 'Male'),
(5, 'rrrrtttttttttttttttt', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `hobbys`
--

CREATE TABLE IF NOT EXISTS `hobbys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `hoby1` text NOT NULL,
  `hoby2` text NOT NULL,
  `hoby3` text NOT NULL,
  `hoby4` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `hobbys`
--

INSERT INTO `hobbys` (`id`, `name`, `hoby1`, `hoby2`, `hoby3`, `hoby4`) VALUES
(1, 'dddddd', 'Cricket', 'Song', '', 'Swimming');

-- --------------------------------------------------------

--
-- Table structure for table `profilepictures`
--

CREATE TABLE IF NOT EXISTS `profilepictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `photo` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `profilepictures`
--

INSERT INTO `profilepictures` (`id`, `name`, `photo`) VALUES
(17, 'ddddddd', 0x766c63736e61702d323031352d30372d30392d31396834346d3234733131322e706e67);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `name`, `email_address`) VALUES
(1, 'gggg', 'gggggggg'),
(2, 'fffff', 'ashikur@ya'),
(3, 'aaaaaaaaaaa', 'anurcse@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `summarys`
--

CREATE TABLE IF NOT EXISTS `summarys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `summarys`
--

INSERT INTO `summarys` (`id`, `company_name`, `summary`) VALUES
(3, 'tttttttttt', 'ttttttttttttttttttttt'),
(4, 'tttttttttttt', 'ttttttttttttttttttttttt');

-- --------------------------------------------------------

--
-- Table structure for table `termsconditions`
--

CREATE TABLE IF NOT EXISTS `termsconditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `condition` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
