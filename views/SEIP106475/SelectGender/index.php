
<?php
include_once("../../../vendor/autoload.php");

use app\Bitm\SEIP106475\SelectGender\Grnder;

$gender = new Grnder();
$genders = $gender->index();
?>



<html>
    <head>
        <title>Select Gender</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success">Add Book Title</button></a><a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Gender</td>
                        <td>Action</td>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($genders as $gender) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $gender->name; ?></td>
                                    <td><?php echo $gender->gender; ?></td>
                                    <td>
                                        <a href="show.php?id=<?php echo $gender->id; ?>" class="btn btn-success">View</a> 
                                        <a href="edit.php?id=<?php echo $gender->id; ?>" class="btn btn-warning"> Edit</a>  
                                        <a href="delete.php?id=<?php echo $gender->id; ?>" class="btn btn-danger">Delete</a> 
                                        <a href="#" class="btn btn-warning">Trash/Recover</a>
                                        <a href="#" class="btn btn-default"> Email To Friend</a>
                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            ?>
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>