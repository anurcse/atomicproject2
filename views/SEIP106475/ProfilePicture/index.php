

<?php

session_start();
include_once("../../../vendor/autoload.php");

use app\Bitm\SEIP106475\ProfilePicture\Profile;
use app\Bitm\SEIP106475\Utility\Utility;

$book = new Profile();
$books = $book->index();
?>



<html>
    <head>
        <title>Profile Picture</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success">Add Book Title</button></a><a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Image</td>
                        <td>Action</td>
                        </thead>
                        <tbody>
                            <?php
                            $slno = 1;
                            foreach ($books as $book) {
                                ?>
                                <tr>
                                    <td><?php echo $slno; ?></td>
                                    <td><?php echo $book->name; ?></td>
                                    <td><?php echo"<img src='images/$book->photo' alt='Mountain View'style='width:100px;height:100px;'>"?></td>

                                    <td>
                                        <a href="show.php?id=<?php echo $book->id;?>" class="btn btn-success"> <span class="glyphicon glyphicon-eye-open"></span> &nbsp;View</a> 
                                        <a href="edit.php?id=<?php echo $book->id;?>" class="btn btn-warning"> <span class="glyphicon glyphicon-pencil"></span> &nbsp; Edit</a>  
                                        <a href="delete.php?id=<?php echo $book->id; ?>" class="btn btn-danger"> <span class="glyphicon glyphicon-remove"></span> &nbsp; Delete</a> 
                                        <a href="#" class="btn btn-warning">  <span class="glyphicon glyphicon-trash"></span> &nbsp; Trash</a>
                                        <a href="#" class="btn btn-default">  <span class="glyphicon glyphicon-envelope"></span> &nbsp; Email To Friend</a>
                                        <a href="#" class="btn btn-default">  <span class="glyphicon glyphicon-send"></span> &nbsp; Email To Friend</a>


                                    </td>
                                </tr>
                                <?php
                                $slno++;
                            }
                            ?>
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>