<?php
session_start();

include_once("../../../vendor/autoload.php");
use app\Bitm\SEIP106475\CheckboxHobby\Hobby;
use \app\BITM\SEIP106475\Utility\Utility;

$obj = new Hobby();
$book = $obj->show($_GET['id']);
?>

<html>
    <head>
        <title>Book Title</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success">Add Book Title</button></a>

                    <select>
                        <option>10</option>
                        <option>20</option>
                        <option>30</option>
                        <option>40</option>
                        <option>50</option>
                    </select>
                    <div id="message">
                        <?php echo Utility::message(); ?>            
                    </div>
                    <a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <td>
                            SL
                        </td>
                        <td>Name</td>
                        <td>Hobby</td>
                        
                        </thead>
                        <tbody>
                           
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $book['name']; ?></td>
                                    <td>
                                    <ul>
                                    <?php
                                    echo "<li>". $book['hoby1']."</li>";echo "<li>".$book['hoby2']."</li>";echo "<li>".$book['hoby3']."</li>";echo "<li>". $book['hoby4']."</li>"; 
                                    ?>
                                    </ul>
                                    </td>

                                    
                                </tr>
                             
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
