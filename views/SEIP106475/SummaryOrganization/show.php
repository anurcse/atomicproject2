<?php
include_once("../../../vendor/autoload.php");

use app\Bitm\SEIP106475\SummaryOrganization\Summary;

$test = new Summary();
$tests = $test->show($_GET['id']);
?>



<html>
    <head>
        <title>Show Summary</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <a href="create.php"><button class="btn btn-success">Add Summary</button></a><a href="#"><button class="btn btn-success" style="float: right;">Dowland as PDF | XL </button></a>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <td>
                            SL
                        </td>
                        <td>Commpany Name</td>
                        <td>Summary</td>
                  
                        </thead>
                        <tbody>
                            
                           
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $tests['company_name'];?></td>
                                    <td><?php echo $tests['summary']; ?></td>
                                
                        </tbody>


                    </table>
                    <div>
                        <ul class="pagination">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>