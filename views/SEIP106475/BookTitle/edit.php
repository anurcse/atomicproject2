<?php

include_once("../../../vendor/autoload.php");
use \app\Bitm\SEIP106475\BookTitle\Book;
use \app\BITM\SEIP106475\Utility\Utility;

$obj = new Book();
$book = $obj->show($_GET['id']);
?>
<html>
    <head>
        <title>Book Title</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <form action="update.php" method="POST">
                        <fieldset>
                            <input  
                           type="hidden" 
                           name="id"
                           value="<?php echo $book['id'];?>"
                           />
                            <label for="title">Book Title:</label>
                            <input  id="title" type="text" 
                                    class="form-inline" name="title" 
                                    size="40"
                                    tabindex="1" required="required"
                                    value="<?php echo $book['title'];?>"/>
                            <label for="title">Author:</label>
                            <input  id="title" type="text" 
                                    class="form-inline" name="author" 
                                    size="40"
                                    tabindex="1" required="required"
                                    value="<?php echo $book['author'];?>"/>
                            <section>
                                <button type="submit" class="btn btn-success">SAVE</button>
                                <button type="submit" class="btn btn-primary">SAVE & Add Again</button>

                                <button type="reset" class="btn btn-info">RESET</button>
                            </section>
                        </fieldset> 
                    </form>

                </div>
            </div>
        </div>
    </body>
</html>