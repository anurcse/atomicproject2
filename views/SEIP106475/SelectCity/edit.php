<?php
session_start();

include_once("../../../vendor/autoload.php");
use app\Bitm\SEIP106475\SelectCity\City;
use \app\BITM\SEIP106475\Utility\Utility;

$obj = new City();
$book = $obj->show($_GET['id']);
?>

<html>
    <head>
        <title>Book Title</title>
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.css" />
    </head>
    <body>

        <div class="container">
            <div class="col-md-8">

            </div>
            <div class="col-md-4">

                <form class="form-inline">
                    <input type="text" class="form-control"  placeholder="Search">
                    <button type="submit" class="btn btn-default ">Search</button>
                </form>
            </div>
            <div class="container ">
                <div class="jumbotron">
                    <form action="update.php" method="POST">
                        <label>Name</label>
                           <input  
                           type="hidden" 
                           name="id"
                           value="<?php echo $book['id'];?>"
                           />
                        <input type="text" name="name" value="<?php echo $book['name'];?>"/>
                            <label for="title">City:</label>
                            <select name="city">
                                <option>----Select Your City----</option>
                                <option value="Dhaka">Dhaka</option>
                                <option value="Tangail">Tangail</option>
                                <option value="Gazipur">Gazipur</option>
                                <option value="Dinajpur">Dinajpur</option>
                                <option value="Mymonsing">Mymonsing</option>
                                <option value="Jamalpue">Jamalpue</option>
                                <option value="Fene">Fene</option>
                                <option value="Lamxpur">Lamxpur</option>
 
                            </select>
Selected City: <?php echo $book['city'];?>
                            <section>
                                <button type="submit" class="btn btn-success">SAVE</button>
                                <button type="submit" class="btn btn-primary">SAVE & Add Again</button>

                                <button type="reset" class="btn btn-info">RESET</button>
                            </section>
                        
                    </form>

                </div>
            </div>
        </div>
    </body>
</html>